﻿#pragma once
#include <vector>
#include <numeric>
#include <iostream>
using namespace std;

class Generator {

    vector<int> sampleResult;
    vector<vector<int>>  sampleTest;

    vector<int> nullVector;
    vector<int> myNumber;
    vector<vector<int>> numberRecursion;
    vector<vector<int>> numberNoRecursion;
    int m,n,k;
    int flag = 1;
    int flag_2 = 1;
    int idxNumber_aboveZeroOne = 0;
    int remainderIDX = 0;
    int sum = 0;
    int endNoRecursion =0;
    int endRecursion = 0;


    
public:

    Generator()
    {
       m = 6;
       n = 4;
       k = 6;
       myNumber.resize(n);
       nullVector.resize(n);
       endNoRecursion = k - int(m / n);
       endRecursion = int(m / n);
    }

    Generator(int m, int n, int k)
    {

        if (m < 0)
        {
            cout << "Incorrect m = " << m << endl;
            exit(0);
        }
        else this->m = m;
        if (n < 1)
        {
            cout << "Incorrect n = " << n << endl;
            exit(0);
        }
        else this->n = n;

        if (k > m) this->k = m;
        else this->k = k;

        nullVector.resize(n);


        if ((this->m % this->n) != 0)
        {
            endNoRecursion = this->k - int(m / n) - 1;
            endRecursion = int(m / n);
        }
        else
        {
            endNoRecursion = this->k - int(m / n);
            endRecursion = int(m / n) - 1;

        }
    }

    void GenerateAll();

    template <class T>
    void Print(T someList) 
    {
      for (auto &x: someList) cout<<" "<<x;
      cout<<endl;
    }

    vector<int>& GetResult(int index);

    void NumberRecursion(int pos, int ost, int k);
    void NumberNoRecursion();
    void InitialVector();
    vector<vector<int>> getNumberNoRecursion();
    vector<vector<int>> getNumberRecursion();
    

};
int Factorial(int n);

