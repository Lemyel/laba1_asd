#include <iostream>
#include "generator.h"
#include <algorithm>
using namespace std;


void PrintVec(vector<vector<int>> Sequence)
{
	for (int i = 0; i < Sequence.size(); i++)
	{
		for (int j = 0; j < Sequence[i].size(); j++)
			cout << Sequence[i][j] << " ";
		cout << endl;
	}
}

int main() 
{
   setlocale(LC_CTYPE, "Russian");
   cout<<"10!="<<Factorial(10)<<endl;
   Generator g;
   g.GenerateAll();
   vector<int> result = g.GetResult(2);
   g.Print(result);
   cout << "==========================" << endl;


   int m = 12;
   int n = 5;
   int k = 8;

   Generator myNumberNoRecursion(m, n, k);
   myNumberNoRecursion.NumberNoRecursion();
   cout << "�� ����������� ����� [" << myNumberNoRecursion.getNumberNoRecursion().size() << "]" << endl;
   //PrintVec(myNumberNoRecursion.getNumberNoRecursion());

   cout << "===========================" << endl;
   Generator myNumberRecursion(m, n, k);
   myNumberRecursion.NumberRecursion(0, m, 0);//pos, m, k(i)=0
   cout << "����������� ����� [" << myNumberRecursion.getNumberRecursion().size() <<"]"<<endl;
   PrintVec(myNumberRecursion.getNumberRecursion());

   return 0;
}