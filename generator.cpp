#include "Generator.h"
using namespace std;

int Factorial(int n)
{
	int f = 1;
	for (int b = 2; b <= n; b++)
	{
		f *= b;
	}
	return f;

}

void Generator::GenerateAll() {
   sampleResult.resize(3);
   sampleResult = vector<int> {1,2,3};
   sampleTest = { {1,2,3},{1,2,4},{1,3,4} };

}

std::vector<int>& Generator::GetResult(int idx) {
	//return sampleResult;
	return sampleTest[idx];
}

void Generator::InitialVector()
{
	myNumber = nullVector;
	if (k > m)
	{
		myNumber[n - 1] = m;
	}
	else if (k <= m)
	{
		myNumber[n - 1] = k;
		for (int i = n - 2; i >= 0; i--)
		{
			if ((m - accumulate(myNumber.rbegin(), myNumber.rend(), 0)) > k)
				myNumber[i] = k;
			else
			{
				myNumber[i] = m - accumulate(myNumber.rbegin(), myNumber.rend(), 0);
				break;
			}
		}
	}
}

//void Generator::NumberRecursion()
//{
//	
//		if (endRecursion < 0) cout << "Incorrect k = " << k << endl;
//	    //���� �� �������� ����������� k, �� ����� return ��� ���������� ���� ����
//		if (!flag_2) return;
//
//		//� ������ IF ������ ���� ���, ���� ��� �������� ������� �������
//		if (flag)
//		{
//			InitialVector();
//			numberRecursion.push_back(myNumber);
//			flag = 0;
//		}
//
//		for (int i = 1; i < n - 1; i++)
//			if (myNumber[i] > 1)
//			{
//				idxNumber_aboveZeroOne = i;
//				break;
//			}
//
//		if (idxNumber_aboveZeroOne == 0)
//		{
//			//��������� k � ������� ����� ������, ���� ������ �� ������� �������(flag_2=0)
//			k--;
//			if (k != endRecursion)
//			{
//				InitialVector();
//				numberRecursion.push_back(myNumber);
//				NumberRecursion();
//				return;
//			}
//			else flag_2=0;
//		}
//		myNumber[idxNumber_aboveZeroOne]--;
//
//		for (int s = idxNumber_aboveZeroOne - 1; s >= 0; s--)
//			myNumber[s] = 0;
//
//		for (int i = idxNumber_aboveZeroOne - 1; i >= 0; i--)
//		{
//			if (m - accumulate(myNumber.cbegin() + i + 1, myNumber.cend(), 0) <= myNumber[i + 1])
//			{
//				myNumber[i] = m - accumulate(myNumber.cbegin() + i + 1, myNumber.cend(), 0);
//				numberRecursion.push_back(myNumber);
//				break;
//			}
//			else
//			{
//				myNumber[i] = myNumber[i + 1];
//				remainderIDX++;
//			}
//		}
//
//		if ((remainderIDX == idxNumber_aboveZeroOne) && (idxNumber_aboveZeroOne == n - 2))
//		{
//			//��������� k, ������� ����� ������ � ���� ��� �����(����� NumberRecursion();), 
//			//���� ������ �� ������� �������(flag_2=0) - ���� ���� �� �������� ����������� k
//			k--;
//			if (k != endRecursion)
//			{
//				InitialVector();
//				numberRecursion.push_back(myNumber);
//				idxNumber_aboveZeroOne = 0;
//				remainderIDX = 0;
//				NumberRecursion();
//				return;
//			}
//			else flag_2=0;
//		}
//
//		idxNumber_aboveZeroOne = 0;
//		remainderIDX = 0;
//		NumberRecursion();
//					
//}

//������� �������, �������, ����������� ����� ������� ����� �������� � ������������������ myVector
void Generator::NumberRecursion(int pos, int ost, int k_from_i)
{
	//�������� �� ��, ����� � ������� ���������� ������� ����� ���� ������� �����
		// ���� ������, i++ � ���������
	if ((n - pos) * k >= ost)
	{
		if (pos == n - 1)
		{
			//�������� �� ��,����� ������������� ������� �� ��� ������ 
			// ������� ��� ���� ������� �� ��� ������ k
			if ((ost < myNumber[pos - 1]) || (ost > k))
			{
				cout << "error" << endl;
				return;
			}
			//����� ���������� ������� � ���� ������������������ ����������
			// ������� ��������� ������� � ���������� ������� ����������� �� 1
			else
			{
				myNumber.push_back(ost);
				numberRecursion.push_back(myNumber);
				myNumber.pop_back();
				return;
			}
		}
		for (int i = k_from_i; i <= k; i++)
		{

			if ((n - pos) * i <= ost)
			{
				myNumber.push_back(i);
				NumberRecursion(pos + 1, ost - i, i);
				myNumber.pop_back();
			}

		}
	}
}
void Generator::NumberNoRecursion()
{
	if (endNoRecursion < 0) cout << "Incorrect k = " << k << endl;
	for (int osnov = 0; osnov <= endNoRecursion; osnov++)
	{
		//������� ������ ������
		k -= ceil(osnov / (osnov+1.0));
		InitialVector();
		numberNoRecursion.push_back(myNumber);
		flag = 1;
		//����� �������� ������� ������� � k=k-0,k-1... ���� ��� ������
		while(flag)
		{
			//���� ����� ������� �� ����� 0 � 1, � ���������� ��� ������(��������� ����� �� �������)
			for (int i = 1; i < n-1 ; i++)
				if (myNumber[i] > 1)
				{
					idxNumber_aboveZeroOne = i;
					break;
				}	
			//���� ����� ����� ���(����� ������ ����������, �� ��� ���� ����� ���� 1), �� ������ ����� �������
			//� ������ k = �- osnov, ���������� �� �����������, �� ��� �� ���������� ��������� ����� � ��������� �������� �������
			if (idxNumber_aboveZeroOne == 0) break;


			//��� ������ �� ����� ��� �����, �������� �� ���� 1, � ���� ��� ���������� �����(������� � myNumber 
			//����� ��� myNumber[idxNumber_aboveZero]  ) �������
			myNumber[idxNumber_aboveZeroOne]--;
		
			//����� myNumber[idxNumber_aboveZero] �������� ��� ��������, ����� ��������� ��������� �� �����
			for (int s = idxNumber_aboveZeroOne -1; s >= 0; s--)
				myNumber[s] = 0;

			sum = accumulate(myNumber.cbegin() + idxNumber_aboveZeroOne, myNumber.cend(), 0);
			//���� ����� ������� ����� �������� ����� myNumber[�] ��������
			for (int i = idxNumber_aboveZeroOne -1; i >=0; i--)
			{
				if (m - sum <= myNumber[i + 1])
				{
					//���� ������� �� ������ myNumber[i+1], myNumber[i]=������� �� �����
					// ����� ���� ��������� myNumber � numberNoRecursion(������ ���� ������������������� myNumber)
					myNumber[i] = m - sum;
					numberNoRecursion.push_back(myNumber);
					break;
				}
				else
				{	
				  //���� �� ������� ������ myNumber[i+1], �� myNumber[i] = ����������� �������� myNumber[i+1]
				  myNumber[i] = myNumber[i + 1];
				  sum += myNumber[i + 1];
				  //remainderIDX ���������� ���������� ����������� ��������, ��� ����� ����� ������ �� ���� �� 
				  //������, ��� �� ��� ��������� ������� ����� idxNumber_aboveZero ������������ ���������� ��������,
				  //  � �� ������� �� �����. ���� ��� ����� ������� ��� ������ ��� ���������� �����, ������ � �������� ����� 
				  //����� ������� ����� �� ����������, ��� ��� ��������� ������� �� ����� ������� �� ����� � ������������������
				  remainderIDX++;
				}
			}
	
			//���� �� �� ������������� ������� ������� � ������� �� ���� �������, �� �� ������ �������� � ����� ������������������
			//������ ��������� ������� �� ����� ������� �� ����� � ������������������, ������������� ���� ������ ����� ������ � k-=1
			// ������� ������ flag =0 � ���� while ����������� � �� �������� ����� ������ � InitialVector()
			if ((remainderIDX == idxNumber_aboveZeroOne)&&(idxNumber_aboveZeroOne ==n-2))
				flag = 0;
			//�������� ������, ����� ���� �������� �� for.. if (myNumber[i] > 1){...} �� ���������, 
			//�������� �� if (idxNumber_aboveZero == 0) break; ���������
			idxNumber_aboveZeroOne = 0;
			//�������� ������ ����� � ����� ������� �� ������ ������� ���������� �������� ������ ��� myNumber[idxNumber_aboveZero] 
			remainderIDX = 0;
		}
	}
}


vector<vector<int>> Generator::getNumberNoRecursion()
{
	return numberNoRecursion;
}

vector<vector<int>> Generator::getNumberRecursion() 
{
	return numberRecursion;
}